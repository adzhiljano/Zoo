package com.example.nasko.zoo.utils;

import com.example.nasko.zoo.models.Pin;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface PinsApiInterface {
    ////There is difference in Asynchronous in Retrofit 1.9 and 2.0 (http://stackoverflow.com/questions/32311531/service-methods-cannot-return-void-retrofit)
    @GET("pins.json")
    Call<List<Pin>> getStreams();

}
