package com.example.nasko.zoo.events;

/**
 * Created by Nasko on 17.4.2016 г..
 */
public class DrawerSectionItemClickedEvent {
    public String section;

    public DrawerSectionItemClickedEvent(String section){
        this.section = section;
    }
}
