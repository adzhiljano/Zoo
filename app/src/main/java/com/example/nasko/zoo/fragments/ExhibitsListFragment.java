package com.example.nasko.zoo.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.nasko.zoo.R;
import com.example.nasko.zoo.activities.ExhibitDetailActivity;
import com.example.nasko.zoo.adapters.ExhibitsAdapter;
import com.example.nasko.zoo.models.Animal;
import com.example.nasko.zoo.utils.AnimalApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ExhibitsListFragment extends ListFragment {

    private ExhibitsAdapter mAdapter;

    public static ExhibitsListFragment getInstance(){
        ExhibitsListFragment fragment = new ExhibitsListFragment();

        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setListShown(false);
        mAdapter = new ExhibitsAdapter(getActivity(), 0);

        //There is a change in the Retrofit API in version 2
        //and since we were unable to get the com.squareup.retrofit:retrofit:1.9.0
        //we should use the Retrofit(RestAdapter class was renamed to Retrofit)
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl((getString(R.string.exhibits_feed)))
                //From Retrofit documentation:
                //By default, Retrofit can only deserialize HTTP bodies into OkHttp's ResponseBody type and it can only accept its RequestBody type for @Body.
                //Converters can be added to support other types. Six sibling modules adapt popular serialization libraries for your convenience.
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AnimalApiInterface animalApiInterface = retrofit.create(AnimalApiInterface.class);

        //There is difference in Asynchronous in Retrofit 1.9 and 2.0 (http://stackoverflow.com/questions/32311531/service-methods-cannot-return-void-retrofit)
        Call<List<Animal>> call = animalApiInterface.getStreams();
        call.enqueue(new Callback<List<Animal>>() {
            @Override
            public void onResponse(Call<List<Animal>> call, Response<List<Animal>> animals) {
                if(animals == null || animals.body() == null || animals.body().isEmpty() || !isAdded()){
                    return;
                }

                for (Animal animal : animals.body()) {
                    mAdapter.add(animal);
                }

                mAdapter.notifyDataSetChanged();
                setListAdapter(mAdapter);
                setListShown(true);
            }

            @Override
            public void onFailure(Call<List<Animal>> call, Throwable t) {
                Log.e("Zoo", "Retrofit error " + t.getMessage());
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(getActivity(), ExhibitDetailActivity.class);
        intent.putExtra(ExhibitDetailActivity.EXTRA_ANIMAL, mAdapter.getItem(position));

        startActivity(intent);
    }
}
