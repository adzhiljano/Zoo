package com.example.nasko.zoo.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.nasko.zoo.R;
import com.example.nasko.zoo.models.Pin;
import com.example.nasko.zoo.utils.PinsApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ZooMapFragment extends SupportMapFragment{

    public static ZooMapFragment getInstance() {
        ZooMapFragment fragment = new ZooMapFragment();

        return  fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(39.7493, -104.9506))
                .zoom(16f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        getMap().animateCamera(CameraUpdateFactory.newCameraPosition(position), null);
        getMap().setMapType(GoogleMap.MAP_TYPE_HYBRID);
        getMap().setTrafficEnabled(true);

        getMap().getUiSettings().setZoomControlsEnabled(true);

        MarkerOptions options = new MarkerOptions().position(new LatLng(39.7493, -104.9506));
        options.title("Zoo");
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        getMap().addMarker(options);

        getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });

        //There is a change in the Retrofit API in version 2
        //and since we were unable to get the com.squareup.retrofit:retrofit:1.9.0
        //we should use the Retrofit(RestAdapter class was renamed to Retrofit)
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl((getString(R.string.pins_feed)))
                        //From Retrofit documentation:
                        //By default, Retrofit can only deserialize HTTP bodies into OkHttp's ResponseBody type and it can only accept its RequestBody type for @Body.
                        //Converters can be added to support other types. Six sibling modules adapt popular serialization libraries for your convenience.
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PinsApiInterface pinApiInterface = retrofit.create(PinsApiInterface.class);

        //There is difference in Asynchronous in Retrofit 1.9 and 2.0 (http://stackoverflow.com/questions/32311531/service-methods-cannot-return-void-retrofit)
        Call<List<Pin>> call = pinApiInterface.getStreams();
        call.enqueue(new Callback<List<Pin>>() {
            @Override
            public void onResponse(Call<List<Pin>> call, Response<List<Pin>> pins) {
                if (pins == null || pins.body() == null || pins.body().isEmpty() || !isAdded()) {
                    return;
                }

                for (Pin pin : pins.body()) {
                    MarkerOptions options = new MarkerOptions().position(new LatLng(pin.getLatitude(), pin.getLongitude()));
                    options.title(pin.getName());
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    getMap().addMarker(options);
                }
            }

            @Override
            public void onFailure(Call<List<Pin>> call, Throwable t) {
                Log.e("Zoo", "Retrofit error " + t.getMessage());
            }
        });
    }
}
