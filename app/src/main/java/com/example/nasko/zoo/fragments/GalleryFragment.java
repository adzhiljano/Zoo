package com.example.nasko.zoo.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.nasko.zoo.R;
import com.example.nasko.zoo.activities.GalleryDetailActivity;
import com.example.nasko.zoo.adapters.GalleryImageAdapter;
import com.example.nasko.zoo.models.GalleryImage;
import com.example.nasko.zoo.utils.GalleryApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GalleryFragment extends Fragment implements AdapterView.OnItemClickListener{

    private GridView mGridView;
    private GalleryImageAdapter mAdapter;

    public static GalleryFragment getInstance() {
        GalleryFragment fragment = new GalleryFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery_grid, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGridView = (GridView)view.findViewById(R.id.grid);
        mGridView.setOnItemClickListener(this);
        mGridView.setDrawSelectorOnTop(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new GalleryImageAdapter(getActivity(), 0);
        mGridView.setAdapter(mAdapter);

        //There is a change in the Retrofit API in version 2
        //and since we were unable to get the com.squareup.retrofit:retrofit:1.9.0
        //we should use the Retrofit(RestAdapter class was renamed to Retrofit)
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl((getString(R.string.gallery_feed)))
                        //From Retrofit documentation:
                        //By default, Retrofit can only deserialize HTTP bodies into OkHttp's ResponseBody type and it can only accept its RequestBody type for @Body.
                        //Converters can be added to support other types. Six sibling modules adapt popular serialization libraries for your convenience.
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GalleryApiInterface galleryApiInterface = retrofit.create(GalleryApiInterface.class);

        //There is difference in Asynchronous in Retrofit 1.9 and 2.0 (http://stackoverflow.com/questions/32311531/service-methods-cannot-return-void-retrofit)
        Call<List<GalleryImage>> call = galleryApiInterface.getStreams();
        call.enqueue(new Callback<List<GalleryImage>>() {
            @Override
            public void onResponse(Call<List<GalleryImage>> call, Response<List<GalleryImage>> galleryImages) {
                if (galleryImages == null || galleryImages.body() == null || galleryImages.body().isEmpty() || !isAdded()) {
                    return;
                }

                for (GalleryImage image : galleryImages.body()) {
                    Log.e("Zoo", image.getThumbnail());
                    mAdapter.add(image);
                }

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<GalleryImage>> call, Throwable t) {
                Log.e("Zoo", "Retrofit error " + t.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GalleryImage image = (GalleryImage)parent.getItemAtPosition(position);
        Intent intent = new Intent(getActivity(), GalleryDetailActivity.class);
        intent.putExtra(GalleryDetailActivity.EXTRA_IMAGE, image.getImage());
        intent.putExtra(GalleryDetailActivity.EXTRA_CAPTION, image.getCaption());
        startActivity(intent);
    }
}
