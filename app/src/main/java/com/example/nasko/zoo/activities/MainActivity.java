package com.example.nasko.zoo.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.nasko.zoo.R;
import com.example.nasko.zoo.events.DrawerSectionItemClickedEvent;
import com.example.nasko.zoo.fragments.ExhibitsListFragment;
import com.example.nasko.zoo.fragments.GalleryFragment;
import com.example.nasko.zoo.fragments.ZooMapFragment;
import com.example.nasko.zoo.utils.EventBus;
import com.squareup.otto.Subscribe;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private String mCurrentFragmentTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //using the ActionBarDrawerToggle constructor with the toolbar as third parameter fixes "Android drawer layout not showing"
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_opened, R.string.drawer_closed) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if(getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(R.string.drawer_opened);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if(getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(R.string.drawer_closed);
                }
            }
        };

        //since setDrawerListener is deprecated, we should use addDrawerListener
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

        displayInitialFragment();
    }

    private void displayInitialFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ExhibitsListFragment.getInstance())
                .commit();

        mCurrentFragmentTitle = getString(R.string.section_exhibits);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onDrawerSectionItemClickEvenet(DrawerSectionItemClickedEvent event) {
        mDrawerLayout.closeDrawers();

        if(event == null || TextUtils.isEmpty(event.section) || event.section.equalsIgnoreCase(mCurrentFragmentTitle)) {
            return;
        }

        Toast.makeText(this, "Section clicked: " + event.section, Toast.LENGTH_SHORT)
                .show();

        if(event.section.equalsIgnoreCase(getString(R.string.section_map))) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, ZooMapFragment.getInstance())
                    .commit();
        } else if(event.section.equalsIgnoreCase(getString(R.string.section_gallery))) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, GalleryFragment.getInstance())
                    .commit();
        } else if(event.section.equalsIgnoreCase(getString(R.string.section_exhibits))) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, ExhibitsListFragment.getInstance())
                    .commit();

        } else {
            return;
        }

        mCurrentFragmentTitle = event.section;
    }
}
