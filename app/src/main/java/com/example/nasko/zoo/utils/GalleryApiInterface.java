package com.example.nasko.zoo.utils;

import com.example.nasko.zoo.models.GalleryImage;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GalleryApiInterface {

    ////There is difference in Asynchronous in Retrofit 1.9 and 2.0 (http://stackoverflow.com/questions/32311531/service-methods-cannot-return-void-retrofit)
    @GET("gallery.json")
    Call<List<GalleryImage>> getStreams();
}
