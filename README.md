Android Tutorial(src: https://github.com/PaulTR/GettingStartedWithAndroid):

- Android Studio(http://developer.android.com/sdk/index.html?gclid=CNeHztX7kswCFRSeGwodsYgBIw)

- Android Activities(http://developer.android.com/guide/components/activities.html)
	-> An Activity is an application component that provides a screen with which users can interact 
	in order to do something, such as dial the phone, take a photo, send an email, or view a map. 
	Each activity is given a window in which to draw its user interface. 
	The window typically fills the screen, but may be smaller than the screen and float on top of other windows.
	
- Relative Layout(http://developer.android.com/guide/topics/ui/layout/relative.html)
	-> RelativeLayout is a view group that displays child views in relative positions.
	  The position of each view can be specified as relative to sibling elements (such as to the left-of or below another view)
	  or in positions relative to the parent RelativeLayout area (such as aligned to the bottom, left or center).
      A RelativeLayout is a very powerful utility for designing a user interface because it can eliminate 
	  nested view groups and keep your layout hierarchy flat, which improves performance. 
	  If you find yourself using several nested LinearLayout groups, you may be able to replace them 
	  with a single RelativeLayout.
	  
- String Resources(http://developer.android.com/guide/topics/resources/string-resource.html)
	-> A string resource provides text strings for your application with optional text styling and formatting
	
- Linear Layout(http://developer.android.com/reference/android/widget/LinearLayout.html)
	-> LinearLayout is a view group that aligns all children in a single direction, vertically or horizontally.
      You can specify the layout direction with the android:orientation attribute.
	  
- DrawerLayout(http://developer.android.com/reference/android/support/v4/widget/DrawerLayout.html)

- ActionBarDrawerToggle(http://developer.android.com/reference/android/support/v4/app/ActionBarDrawerToggle.html)

- ListView(http://developer.android.com/guide/topics/ui/layout/listview.html)
	-> ListView is a view group that displays a list of scrollable items. The list items are automatically inserted to the list 
	  using an Adapter that pulls content from a source such as an array or database query and converts each 
	  item result into a view that's placed into the list.
	  
- EventBus(https://github.com/square/otto)
	-> An enhanced Guava-based event bus with emphasis on Android support

- Activity Lifecycle(http://developer.android.com/training/basics/activity-lifecycle/index.html && http://developer.android.com/reference/android/app/Activity.html)

- Fragment(http://developer.android.com/guide/components/fragments.html)
	-> A Fragment represents a behavior or a portion of user interface in an Activity. You can combine multiple fragments in a 
	  single activity to build a multi-pane UI and reuse a fragment in multiple activities. You can think of a fragment as a 
	  modular section of an activity, which has its own lifecycle, receives its own input events, and which you can add or 
	  remove while the activity is running (sort of like a "sub activity" that you can reuse in different activities).
	  
- Gradle, please(http://gradleplease.appspot.com/)

- Fragment Manager(http://developer.android.com/reference/android/app/FragmentManager.html)
	-> Interface for interacting with Fragment objects inside of an Activity
	
- Retrofit(http://square.github.io/retrofit/)
	-> A type-safe HTTP client for Android and Java
	
- Picasso(http://square.github.io/picasso/)
	-> Images add much-needed context and visual flair to Android applications. Picasso allows for hassle-free image loading in your application—often in one line of code!
	
- Intents and Intent Filters(http://developer.android.com/guide/components/intents-filters.html)
	-> An Intent is a messaging object you can use to request an action from another app component. 
	
- ScrollView(http://developer.android.com/reference/android/widget/ScrollView.html)
	-> Layout container for a view hierarchy that can be scrolled by the user, allowing it to be larger than the physical display.
	   A ScrollView is a FrameLayout, meaning you should place one child in it containing the entire contents to scroll;
	   this child may itself be a layout manager with a complex hierarchy of objects. A child that is often used is a LinearLayout 
	   in a vertical orientation, presenting a vertical array of top-level items that the user can scroll through.
	   
- ImageView ScaleType(http://developer.android.com/reference/android/widget/ImageView.ScaleType.html)
	-> Options for scaling the bounds of an image to the bounds of this view.
	
- Google Maps Android API(https://developers.google.com/maps/documentation/android-api/start)

- SupportMapFragment(https://developers.google.com/android/reference/com/google/android/gms/maps/SupportMapFragment)
	-> A Map component in an app. This fragment is the simplest way to place a map in an application. It's a wrapper around a view of a map to automatically handle the necessary life cycle needs. 